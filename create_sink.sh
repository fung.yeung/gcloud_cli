# create logging sink from a source project to a destination project in BigQuery

gcloud logging sinks create bigquery_export \
bigquery.googleapis.com/projects/cn-ops-spdigital/datasets/monitoring \
--description="export of all logs from bigquery" \
--log-filter='resource.type="bigquery_project" OR resource.type="bigquery_dataset"' \
--use-partitioned-tables \
--project=cn-imt-water-esight